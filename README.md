**نمونه قرارداد پروژه‌ی برنامه‌نویسی سایت**

این قرارداد و بند‌های آن بر اساس تجربه‌های شخصی اضافه شدن، خوشحال میشم شما هم بندهایی که کمک میکنه رو اضافه کنید به این قرارداد تا به مرور کامل‌تر بشه

[مشاهده نسخه‌ی MarkDown](https://framagit.org/ordinarySaleh/agreement-sample/-/blob/master/GharardadSample.md) |
[دانلود نسخه‌ی odt](https://framagit.org/ordinarySaleh/agreement-sample/-/blob/master/GharardadSample.odt)  | 
[دانلود نسخه‌ی docx](https://framagit.org/ordinarySaleh/agreement-sample/-/blob/master/GharardadSample.docx) |
[دانلود نسخه‌ی pdf](https://framagit.org/ordinarySaleh/agreement-sample/-/blob/master/GharardadSample.pdf) 